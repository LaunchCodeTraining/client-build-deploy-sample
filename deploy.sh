#! /usr/bin/env bash -e

acl_default=public-read
bucket_name_default=patrick-client-bucket-nga-c6
bucket_region_default=us-east-1

# set from env vars or use defaults
acl=${ACL:=$acl_default}
bucket_name=${BUCKET_NAME:=$bucket_name_default}
bucket_region=${AWS_DEFAULT_REGION:=$bucket_region_default}

# deploy to bucket
printf "\ndeploying [build/] to [$bucket_name_default] bucket in region [$bucket_region] with ACL [$acl]\n\n"

aws s3 cp --recursive build/ "s3://$bucket_name/" --acl "$acl"

printf "\nsite deployed to: http://${bucket_name}.s3-website.${bucket_region}.amazonaws.com\n\n"