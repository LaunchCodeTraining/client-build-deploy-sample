#! /usr/bin/env bash

set -e

# -- USAGE -- #
# set the following environment variables before calling this script

# ENV_MODULE_PATH: path to env.js module file relative to src/ directory (ex - modules/env.js)

# API_ORIGIN: the origin of the API (ex - http://<public DNS name>:PORT)
# GEOSERVER_ORIGIN: the origin of the GeoServer (ex - http://<public DNS name>:PORT)
# origins can not include any trailing path (even '/')
# origin port should only be set if the exposed port is not the HTTP default of 80

# ----- LOCAL VARS ------ #

# defaults used if environment variables are not set before running the script
# defaults are after :=
env_module_path="${ENV_MODULE_PATH:=modules/env.js}"
api_origin="${API_ORIGIN:=http://localhost:8080}"
geoserver_origin="${GEOSERVER_ORIGIN:=http://localhost:8081}"

# ----- OPENLAYERS SOURCE REPLACEMENT ----- #

html_target=build/index.html
# targets (in index.html) to substitute the CDN minified openlayers JS/CSS sources for deployment

# <script src="openlayers/ol.js"></script>
ol_js_target=openlayers/ol.js

# <link rel="stylesheet" href="openlayers/ol.css" />
ol_css_target=openlayers/ol.css 

# the replacements are the respective CDN links
# so the above (local sources) become the below (CDN minified sources)

# <script src="https://cdnjs.cloudflare.com/ajax/libs/openlayers/4.6.5/ol.js"></script>
ol_js_replacement=https://cdnjs.cloudflare.com/ajax/libs/openlayers/4.6.5/ol.js

# <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/openlayers/4.6.5/ol.css" />
ol_css_replacement=https://cdnjs.cloudflare.com/ajax/libs/openlayers/4.6.5/ol.css

# ----- END VARS ----- #

# remove old build directory if present
rm -rf build/

# copy src content to the build dir
# so any deployment related changes do not affect the local source code
cp -R src/ build/

# substitute env var values for the environment module file
cat <<EOF > "build/$env_module_path"
export default {
  API_ORIGIN: "$api_origin",
  GEOSERVER_ORIGIN: "$geoserver_origin",
}; 
EOF

# substitute the openlayers debug CSS/JS file path with their minified CDN equivalents

# on macs the .tmp is required to provide a file extension for the temporary file generated during in-place replacement with sed
# by using -i.tmp this will work on mac or linux, thanks to https://stackoverflow.com/a/22084103/7542831

# we issue two sed replacement commands separated by ';'
# we use '~' as the delimeter to not conflict with the slashes used in target/replacement

# execute the replacements
sed -i.tmp "s~$ol_css_target~$ol_css_replacement~g;s~$ol_js_target~$ol_js_replacement~g" "$html_target" \
&& rm "${html_target}.tmp" \
&& rm -rf build/openlayers
 
printf "\nbuild complete\n\n"

exit 0