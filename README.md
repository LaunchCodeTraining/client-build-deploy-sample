# Client Build and Deliver/Deploy

The two scripts `build.sh` and `deploy.sh` are used to build the client code and deliver/deploy it to your S3 bucket.

The purpose of these steps is to mitigate having to manually set the API and Geoserver origin values (between local and deployed values). It also adds some additional conveniences outlined below.

Normally this "building" is handled by a build tool like WebPack. But because this codebase is so simple we can avoid the overhead of third party package installation and configuration by writing a few scripts!

> NOTE!

**Make sure to add a `.gitignore` file with `build/` in it to ignore the temporary build directory. it is derived code that does not belong in your source control**

you can run this command to generate it

```sh
# in the root directory of your client repo
$ echo "build/" >> .gitignore
```

# Build Step

The build step is called using the `build.sh` script. The following environment variables can be set before calling it.

## Environment Variables

> optional: `ENV_MODULE_PATH=modules/env.js`

this is the path (relative to `src/`) where the environment file lives. it will hold the origin values for the API and Geoserver. the default expected location is `src/modules/env.js`.

here is how it would look locally in `src/modules/env.js`

```js
export default {
  API_ORIGIN: "http://localhost:8080",
  GEOSERVER_ORIGIN: "http://localhost:8081",
};
```

using the env module is just accessing the value you need

```js
import env from "./env.js";

// example to build the report dates list endpoint
const datesEndpoint = env.API_ORIGIN + "/reports/dates";
```

> **required:** `API_ORIGIN`

this is the deployed API origin. you can get this from your public DNS name of the EC2 instance. it will look like `http://<public DNS name>:8080` or `http://<load balancer public DNS name>` if you are using a load balancer.

> **required:** `GEOSERVER_ORIGIN`

this is the deployed Geoserver origin. you can find it in the same way as the API origin.

## Calling the build script

> if you are using Jenkins you can set the environment variables as build parameters then just call: `bash build.sh`

> if you use the default `src/modules/env.js` location you do not need to set the `ENV_MODULE_PATH` environment variable

> here is an example of running the script locally (setting the env vars in-line)

```sh
# where 'ec2-18-224-137-43.us-east-2' would be your public DNS names
# notice this is one single statement spread across multiple lines using the '\' new line escape
$ API_ORIGIN=http://ec2-18-224-137-43.us-east-2.compute.amazonaws.com:8080 \
GEOSERVER_ORIGIN=http://ec2-18-224-137-43.us-east-2.compute.amazonaws.com:8081 \
bash build.sh
```

# What the build script does

## copy `src/` dir to `build/` dir

we copy this over so that the modifications made to the build directory do not affect your source code. your source code in `src/` are used for local development and checking into version control. the `build/` dir is ignored in `.gitignore` since it is derived code that is temporary and only used for deployment.

## replace `env.js` file with deployed origins

replaces the API and Geoserver origins in `build/modules/env.js` with the values set by the environment variables `API_ORIGIN` and `GEOSERVER_ORIGIN`

> from this (local in `src/modules/env.js`)

```js
// these are your local values
export default {
  API_ORIGIN: "http://localhost:8080",
  GEOSERVER_ORIGIN: "http://localhost:8081",
};
```

> to this (deployment directory in `build/modules/env.js`)

> when the following environment variables are set

```sh
API_ORIGIN=http://<public API DNS name>:8080
GEOSERVER_ORIGIN=http://<public Geoserver DNS name>:8081
```

```js
export default {
  API_ORIGIN: "http://<public API DNS name>:8080",
  GEOSERVER_ORIGIN: "http://<public Geoserver DNS name>:8081",
};
```

## replace OpenLayers debug with CDN links

replaces the OpenLayers CSS and JS debug file links with their respective minified CDN links in `build/index.html`. locally it helps to have the debug code in `src/openlayers/`. but in production (deployment) you want to use the minified versions from the CDN links. the minified versions have an appreciable saving in size and because they are served over CDN they will be cached and served quicker.

> from this (local in `src/index.html`)

```html
<!-- openlayers sources -->
<script src="openlayers/ol.js"></script>
<link rel="stylesheet" href="openlayers/ol.css" />
```

> to this (deployed in `build/index.html`)

```html
<!-- openlayers local sources -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/openlayers/4.6.5/ol.js"></script>
<link
  rel="stylesheet"
  href="https://cdnjs.cloudflare.com/ajax/libs/openlayers/4.6.5/ol.css"
/>
```

# Deploy Step

the deploy step sends the `build/` directory to your S3 bucket

## Environment Variables

> optional: `ACL=public-read`

the access control list for the objects sent to the bucket. defaults to `public-read` (**you should have no reason to change this**)

> optional: `BUCKET_REGION=us-east-1`

the name of the region your bucket exists in. defaults to `us-east-1` (**you likely do not need to change this**)

> **required:** `BUCKET_NAME`

the name of your S3 bucket to send the `build/` dir

## Using the deploy script

After the build script is done (in its own pipeline stage to protect against failure) you can deploy. Likely you only need to set the `BUCKET_NAME` environment variable for this script:

```sh
$ BUCKET_NAME=my-bucket-name bash deploy.sh

# will output the link to your deployed site
```
